package com.safira.demo.api;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class PetApiNativeTest extends PetApiTest {

  // Execute the same tests but in native mode.
}
