package com.safira.demo.api;

import com.safira.demo.model.Pet;
import java.io.InputStream;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/pet")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJAXRSSpecServerCodegen")
public interface PetApi {

  @POST
  @Consumes({"application/json", "application/xml"})
  Response addPet(@Valid @NotNull Pet body);

  @DELETE
  @Path("/{petId}")
  Response deletePet(@PathParam("petId") Long petId, @HeaderParam("api_key") String apiKey);

  @GET
  @Path("/findByStatus")
  @Produces({"application/xml", "application/json"})
  Response findPetsByStatus(@QueryParam("status") @NotNull List<String> status);

  @GET
  @Path("/findByTags")
  @Produces({"application/xml", "application/json"})
  Response findPetsByTags(@QueryParam("tags") @NotNull List<String> tags);

  @GET
  @Path("/{petId}")
  @Produces({"application/xml", "application/json"})
  Response getPetById(@PathParam("petId") Long petId);

  @PUT
  @Consumes({"application/json", "application/xml"})
  Response updatePet(@Valid @NotNull Pet body);

  @POST
  @Path("/{petId}")
  @Consumes({"application/x-www-form-urlencoded"})
  Response updatePetWithForm(
      @PathParam("petId") Long petId,
      @FormParam(value = "name") String name,
      @FormParam(value = "status") String status);

  @POST
  @Path("/{petId}/uploadImage")
  @Consumes({"multipart/form-data"})
  @Produces({"application/json"})
  Response uploadFile(
      @PathParam("petId") Long petId,
      @FormParam(value = "additionalMetadata") String additionalMetadata,
      @FormParam(value = "file") InputStream fileInputStream);
}
