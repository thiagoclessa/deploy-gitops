package com.safira.demo.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonValue;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.Objects;
import javax.validation.Valid;
import javax.validation.constraints.*;

@JsonTypeName("Order")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJAXRSSpecServerCodegen")
public class Order implements Serializable {

  private @Valid Long id;
  private @Valid Long petId;
  private @Valid Integer quantity;
  private @Valid OffsetDateTime shipDate;
  private @Valid Boolean complete = false;
  private @Valid BigDecimal price;

  public enum StatusEnum {
    PLACED(String.valueOf("placed")),
    APPROVED(String.valueOf("approved")),
    DELIVERED(String.valueOf("delivered"));

    private String value;

    StatusEnum(String v) {
      value = v;
    }

    public String value() {
      return value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static StatusEnum fromValue(String value) {
      for (StatusEnum b : StatusEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  private @Valid StatusEnum status;

  /** */
  public Order id(Long id) {
    this.id = id;
    return this;
  }

  @JsonProperty("id")
  public Long getId() {
    return id;
  }

  @JsonProperty("id")
  public void setId(Long id) {
    this.id = id;
  }

  /** */
  public Order petId(Long petId) {
    this.petId = petId;
    return this;
  }

  @JsonProperty("petId")
  public Long getPetId() {
    return petId;
  }

  @JsonProperty("petId")
  public void setPetId(Long petId) {
    this.petId = petId;
  }

  /** */
  public Order quantity(Integer quantity) {
    this.quantity = quantity;
    return this;
  }

  @JsonProperty("quantity")
  public Integer getQuantity() {
    return quantity;
  }

  @JsonProperty("quantity")
  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  /** */
  public Order shipDate(OffsetDateTime shipDate) {
    this.shipDate = shipDate;
    return this;
  }

  @JsonProperty("shipDate")
  public OffsetDateTime getShipDate() {
    return shipDate;
  }

  @JsonProperty("shipDate")
  public void setShipDate(OffsetDateTime shipDate) {
    this.shipDate = shipDate;
  }

  /** */
  public Order complete(Boolean complete) {
    this.complete = complete;
    return this;
  }

  @JsonProperty("complete")
  public Boolean getComplete() {
    return complete;
  }

  @JsonProperty("complete")
  public void setComplete(Boolean complete) {
    this.complete = complete;
  }

  /** Order Price */
  public Order price(BigDecimal price) {
    this.price = price;
    return this;
  }

  @JsonProperty("price")
  public BigDecimal getPrice() {
    return price;
  }

  @JsonProperty("price")
  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  /** Order Status */
  public Order status(StatusEnum status) {
    this.status = status;
    return this;
  }

  @JsonProperty("status")
  public StatusEnum getStatus() {
    return status;
  }

  @JsonProperty("status")
  public void setStatus(StatusEnum status) {
    this.status = status;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Order order = (Order) o;
    return Objects.equals(this.id, order.id)
        && Objects.equals(this.petId, order.petId)
        && Objects.equals(this.quantity, order.quantity)
        && Objects.equals(this.shipDate, order.shipDate)
        && Objects.equals(this.complete, order.complete)
        && Objects.equals(this.price, order.price)
        && Objects.equals(this.status, order.status);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, petId, quantity, shipDate, complete, price, status);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Order {\n");

    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    petId: ").append(toIndentedString(petId)).append("\n");
    sb.append("    quantity: ").append(toIndentedString(quantity)).append("\n");
    sb.append("    shipDate: ").append(toIndentedString(shipDate)).append("\n");
    sb.append("    complete: ").append(toIndentedString(complete)).append("\n");
    sb.append("    price: ").append(toIndentedString(price)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
