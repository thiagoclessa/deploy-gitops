package com.safira.demo.api;

import com.safira.demo.model.User;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/user")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJAXRSSpecServerCodegen")
public interface UserApi {

  @POST
  Response createUser(@Valid @NotNull User body);

  @POST
  @Path("/createWithArray")
  Response createUsersWithArrayInput(@Valid @NotNull List<User> body);

  @POST
  @Path("/createWithList")
  Response createUsersWithListInput(@Valid @NotNull List<User> body);

  @DELETE
  @Path("/{username}")
  Response deleteUser(@PathParam("username") String username);

  @GET
  @Path("/{username}")
  @Produces({"application/xml", "application/json"})
  Response getUserByName(@PathParam("username") String username);

  @GET
  @Path("/login")
  @Produces({"application/xml", "application/json"})
  Response loginUser(
      @QueryParam("username") @NotNull String username,
      @QueryParam("password") @NotNull String password);

  @GET
  @Path("/logout")
  Response logoutUser();

  @PUT
  @Path("/{username}")
  Response updateUser(@PathParam("username") String username, @Valid @NotNull User body);
}
