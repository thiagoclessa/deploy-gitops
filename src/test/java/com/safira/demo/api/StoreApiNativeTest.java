package com.safira.demo.api;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class StoreApiNativeTest extends StoreApiTest {

  // Execute the same tests but in native mode.
}
