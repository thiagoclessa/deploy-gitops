package com.safira.demo.api;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class UserApiNativeTest extends UserApiTest {

  // Execute the same tests but in native mode.
}
