package com.safira.demo.api;

import com.safira.demo.model.Order;
import javax.validation.Valid;
import javax.validation.constraints.*;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/store")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJAXRSSpecServerCodegen")
public interface StoreApi {

  @DELETE
  @Path("/order/{orderId}")
  Response deleteOrder(@PathParam("orderId") @Min(1L) Long orderId);

  @GET
  @Path("/inventory")
  @Produces({"application/json"})
  Response getInventory();

  @GET
  @Path("/order/{orderId}")
  @Produces({"application/xml", "application/json"})
  Response getOrderById(@PathParam("orderId") @Min(1L) @Max(10L) Long orderId);

  @POST
  @Path("/order")
  @Produces({"application/xml", "application/json"})
  Response placeOrder(@Valid @NotNull Order body);
}
